import './App.css'



function Film(props) {
  return(
    <>
      <div className="film-card">
        <div className="image"><img className='image' src={props.url}/></div>
        <div className="name">{props.name}</div>
        <div className="date">{props.date}</div>
        <a href={props.iurl} className = "btn" target="_blank">Image</a>
      </div>
    </>
    
  );
}

function App() {
  return (
    <div className='list-wrapper'>
      
      <Film 
        image url = "https://static.kinoafisha.info/k/movie_posters/canvas/800x1200/upload/movie_posters/3/5/3/5280353/556774001653465014.jpg"
        name = "Shutter Island"
        date = "2010"
        iurl = "https://static.kinoafisha.info/k/movie_posters/canvas/800x1200/upload/movie_posters/3/5/3/5280353/556774001653465014.jpg"/>
      <Film 
       image url = "http://film-like.com/images/film/full/69/58476.jpg"
       name = "The Intouchables" 
       date = "2011"
       iurl = "http://film-like.com/images/film/full/69/58476.jpg"/>
      <Film 
       image url = "https://i.pinimg.com/474x/5e/f9/59/5ef9598d7f1e3cfa5f16951cd794dfdf.jpg"
       name ="Doctor Strange" 
       date = "2016"
       iurl = "https://i.pinimg.com/474x/5e/f9/59/5ef9598d7f1e3cfa5f16951cd794dfdf.jpg"/>
      
    </div>
   
  );
}

export default App;
